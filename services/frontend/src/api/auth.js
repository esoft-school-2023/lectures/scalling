export const login = async(username, password) => {
  const {data: {accessToken}} = await window.httpApi.post("/auth", {
    type: "credentials",
    username,
    password
  });

  window.setToken(accessToken);

  return await getResource();
};

export const logout = async() => {
  await window.httpApi.delete("/auth");

  window.setToken(null);
};

export const getResource = async() => {
  const {data: user} = await window.httpApi.get("/auth");

  return user;
};

export default {login, logout, getResource};
