export const getPlayers = () => new Promise((resolve, reject) => {
  window.socket.emit("players:get-players", true, (result) => {
    if (result?.message) {
      reject(result);
    } else {
      resolve(result);
    }
  });
});

export default {getPlayers};
