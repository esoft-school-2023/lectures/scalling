import axios from "axios";
import socketIO from "socket.io-client";

const axiosConfig = {
  baseURL: process.env.REACT_APP_API_URL,
  withCredentials: true,
  timeout: 0
};
let accessToken = null;

window.setToken = (token) => {
  accessToken = token;
};

window.httpApi = axios.create(axiosConfig);

window.httpApi.interceptors.request.use((request) => {
  if (!request.headers.Authorization && accessToken) {
    request.headers.Authorization = `Bearer ${accessToken}`;
  }

  return request;
});
window.httpApi.interceptors.response.use(
  (response) => response,
  async(error) => {
    const {response, config} = error;

    if (response?.status !== 401) {
      throw error;
    }

    const {data: {accessToken}} = await axios.post("/auth", {type: "refreshToken"}, axiosConfig);

    window.setToken(accessToken);

    return window.httpApi(config);
  }
);
window.httpApi.interceptors.response.use(
  (response) => response,
  (error) => {
    const {response} = error;

    switch(response?.status) {
      case 401:
        throw new Error("unauthorized");
      case 404:
        throw new Error("not_found");
      default:
        throw new Error("unknown_error")
    }
  }
);

window.socket = socketIO.io(process.env.REACT_APP_API_URL, {autoConnect: false});

window.socket.on("unauthorized", async(authError, cb) => {
  if (authError === "invalid_token") {
    accessToken = null;
  }
  if (accessToken) {
    return cb(accessToken);
  }

  try {
    const {data: {accessToken}} = await axios.post("/auth", {type: "refreshToken"}, axiosConfig);

    window.setToken(accessToken);

    return cb(accessToken);
  } catch(err) {
    window.socket.disconnect();

    throw err;
  }
});

const emit = window.socket.emit.bind(window.socket);

window.socket.emit = (...event) => {
  window.socket.connect();

  emit(...event);
};
