import * as React from "react";
import * as mobx from "mobx";
import authApi from "../api/auth";

export class UserStore {
  user = null;

  constructor() {
    mobx.makeObservable(this, {
      user: mobx.observable
    });
  }

  init = async() => {
    try {
      const user = await authApi.getResource();

      mobx.runInAction(() => {
        this.user = user;
      });
    } catch(_) {
      mobx.runInAction(() => {
        this.user = null;
      });
    }
  };

  login = async(username, password) => {
    const user = await authApi.login(username, password);

    mobx.runInAction(() => {
      this.user = user;
    });
  };

  logout = async() => {
    await authApi.logout();

    mobx.runInAction(() => {
      this.user = null;
    });
  };
}

export const UserStoreContext = React.createContext(null);
