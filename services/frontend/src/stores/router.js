import * as React from "react";
import * as mobx from "mobx";

export class RouterStore {
  history = null;
  location = null;

  constructor(history) {
    this.history = history;

    mobx.makeObservable(this, {
      location: mobx.observable,
      updateLocation: mobx.action
    });

    const unsubscribeFromHistory = history.listen(({location}) => {
      this.updateLocation(location);
    });

    this.updateLocation(history.location);

    history.subscribe = (listener) => {
      const onStoreChange = () => {
        listener(mobx.toJS(this.location), history.action);
      };

      const unsubscribeFromStore = mobx.observe(this, 'location', onStoreChange, false);

      listener(this.location, history.action);

      return unsubscribeFromStore;
    };
    history.unsubscribe = unsubscribeFromHistory;
  }

  updateLocation = (location) => {
    this.location = location;
  };

  push = (location, state) => {
    this.history.push(location, state);
  };

  replace = (location, state) => {
    this.history.replace(location, state);
  };

  go = (n) => {
    this.history.go(n);
  };

  goBack = () => {
    this.history.back();
  };

  goForward = () => {
    this.history.forward();
  };

  createHref = (to) => {
    this.history.createHref(to);
  };
}

export const RouterStoreContext = React.createContext(null);
