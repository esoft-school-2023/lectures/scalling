import * as React from "react";
import {Button as MuiButton} from "@mui/material";

const Button = ({children, color, onClick, disabled, fill}) => (
  <MuiButton
    sx={{width: fill ? "100%" : undefined}}
    variant={"contained"}
    color={color}
    onClick={onClick}
    disabled={disabled}
  >
    {children}
  </MuiButton>
);

export default React.memo(Button);
