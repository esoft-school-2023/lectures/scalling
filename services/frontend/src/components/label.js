import * as React from "react";
import {Box} from '@mui/material';

const Label = ({sx, color, children}) => {
  return (
    <Box sx={{
      ...sx,
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      padding: "4px 12px",
      borderRadius: "6px",
      background: (theme) => theme.palette[color].main,
      color: (theme) => theme.palette[color].contrastText
    }}>
      {children}
    </Box>
  );
};

export default React.memo(Label);
