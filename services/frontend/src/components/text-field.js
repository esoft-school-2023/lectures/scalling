import * as React from "react";
import {TextField as MuiTextField} from "@mui/material";

const TextField = ({
  type = "text",
  value,
  error,
  helperText,
  placeholder,
  onChange
}) => (
  <MuiTextField
    variant={"outlined"}
    type={type}
    placeholder={placeholder}
    defaultValue={value}
    error={error}
    helperText={helperText}
    onChange={(e) => onChange(e.target.value)}
  />
);

export default React.memo(TextField);
