import * as React from "react";
import {
  Table as MuiTable,
  TableBody,
  TableHead,
  TableRow,
  TableCell
} from "@mui/material";

const Table = ({
  sx,
  headSx,
  headRowSx,
  bodySx,
  columns,
  rowKey,
  rows,
  withHead = true,
  withoutBorders = true
}) => (
  <MuiTable sx={{
    ...sx,
    border: withoutBorders ? "none" : undefined
  }}>
    {
      withHead && (
        <TableHead sx={{
          ...headSx,
          border: withoutBorders ? "none" : undefined
        }}>
          <TableRow sx={{
            ...headRowSx,
            border: withoutBorders ? "none" : undefined
          }}>
            {
              columns.map(({name, title, sx, valueSx: _valueSx, ...props}) => (
                <TableCell
                  key={name}
                  sx={{
                    ...sx,
                    border: withoutBorders ? "none" : undefined
                  }}
                  {...props}
                >
                  {title}
                </TableCell>
              ))
            }
          </TableRow>
        </TableHead>
      )
    }
    {
      rows && (
        <TableBody sx={{
          ...bodySx,
          border: withoutBorders ? "none" : undefined
        }}>
          {
            rows.map((row) => (
              <TableRow
                key={row[rowKey]}
                sx={{border: withoutBorders ? "none" : undefined}}
              >
                {
                  columns.map((column) => (
                    <TableCell
                      key={column.name}
                      sx={{
                        ...column.valueSx,
                        border: withoutBorders ? "none" : undefined
                      }}
                    >
                      {column.format(row)}
                    </TableCell>
                  ))
                }
              </TableRow>
            ))
          }
        </TableBody>
      )
    }
  </MuiTable>
);

export default React.memo(Table);
