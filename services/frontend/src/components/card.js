import * as React from "react";
import {
  Card as MuiCard,
  CardHeader,
  CardContent
} from "@mui/material";

const Card = ({sx, headerSx, header, contentSx, children}) => (
  <MuiCard sx={sx}>
    {header && <CardHeader sx={headerSx} title={header} />}
    <CardContent sx={contentSx}>
      {children}
    </CardContent>
  </MuiCard>
);

export default React.memo(Card);
