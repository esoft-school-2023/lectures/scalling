import React from "react";
import {Box, Icon, ListItem} from "@mui/material";
import {NavLink, Outlet} from "react-router-dom";
import {observer} from "mobx-react-lite";
import {UserStoreContext} from "../../stores/user";
import {RouterStoreContext} from "../../stores/router";
import ExitIcon from "./exit-icon";
import LogoIcon from "./logo-icon";

const Navbar = () => {
  const userStore = React.useContext(UserStoreContext);
  const routerStore = React.useContext(RouterStoreContext);

  return (
    <React.Fragment>
      <Box
        component={"nav"}
        sx={{
          width: "100%",
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          background: "#FFFFFF",
          boxShadow: "0 2px 6px rgba(44, 57, 121, 0.1)",
          borderRadius: "0 0 16px 16px",
          padding: "16px 40px"
        }}
      >
        <Icon>
          <LogoIcon />
        </Icon>
        <Box
          component={"li"}
          sx={{
            display: "flex",
            flexDirection: "row"
          }}
        >
          <ListItem
            sx={{
              padding: "4px 12px",
              borderRadius: "200px",
              color: routerStore.location.pathname === "/players" ?
                "#FFFFFF" :
                "#373745",
              background: routerStore.location.pathname === "/players" ?
                "#60C2AA" :
                undefined
            }}
            component={NavLink}
            to={"/players"}
          >
            {"Активные игроки"}
          </ListItem>
        </Box>
        <Icon
          sx={{
            fill: "#373745",
            ":hover": {
              fill: "#3BA189",
              cursor: "pointer"
            }
          }}
          onClick={userStore.logout}
        >
          <ExitIcon />
        </Icon>
      </Box>
      <Box sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#F6F6F6",
        minHeight: "100vh"
      }}>
        <Outlet />
      </Box>
    </React.Fragment>
  );
};

export default observer(Navbar);
