import * as React from "react";
import {CircularProgress} from "@mui/material";
import {Route, Router, Routes, Navigate} from "react-router";
import {observer} from "mobx-react-lite";
import {RouterStoreContext} from "./stores/router";
import {UserStoreContext} from "./stores/user";
import Navbar from "./components/navbar";
import LoginPage from "./pages/login";
import PlayersPage from "./pages/players";

const App = () => {
  const routerStore = React.useContext(RouterStoreContext);
  const userStore = React.useContext(UserStoreContext);

  React.useEffect(() => {
    userStore.init();
  }, [userStore]);

  return (
    <React.Suspense fallback={<CircularProgress />}>
      <Router location={routerStore.location} navigator={routerStore.history}>
        <Routes>
          <Route
            path={"/login"}
            element={<LoginPage />}
          />
          <Route path={"/"} element={<Navbar />}>
            <Route
              index={true}
              element={<Navigate to={"players"} replace={true} />}
            />
            <Route
              path={"players"}
              element={<PlayersPage />}
            />
          </Route>
        </Routes>
      </Router>
    </React.Suspense>
  );
};

export default observer(App);
