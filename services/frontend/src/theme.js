import {createTheme} from "@mui/material";

export const theme = createTheme({
  palette: {
    text: {
      contrastText: "#373745"
    },
    primary: {
      light: "#60C2AA",
      main: "#60C2AA",
      dark: "#3BA189",
      contrastText: "#FFFFFF"
    },
    secondary: {
      light: "#F7F7F7",
      main: "#F7F7F7",
      dark: "#DCDCDF",
      contrastText: "#373745"
    },
    success: {
      main: "#A8D37F",
      contrastText: "#FFFFFF"
    },
    error: {
      main: "#EC7777",
      contrastText: "#FFFFFF"
    },
    warning: {
      main: "#87CAE8",
      contrastText: "#FFFFFF"
    },
    info: {
      main: "#EDEDED",
      contrastText: "#373745"
    },
    contrastThreshold: 4.5
  },
  typography: {
    fontFamily: "'Roboto'",
    fontSize: 16,
    fontWeight: "400",
    lineHeight: "24px",
    h1: {
      fontSize: 24,
      fontWeight: "700",
      lineHeight: "36px"
    },
    button: {
      fontSize: 16,
      fontWeight: "500",
      lineHeight: "24px"
    }
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: `
        @font-face {
          font-family: 'Roboto';
          font-style: normal;
          font-display: swap;
          font-weight: 400;
          src: local('Roboto'), local('Roboto-Regular');
          unicodeRange: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF;
        }
      `
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          boxSizing: "border-box",
          ":hover": {
            "& .MuiOutlinedInput-notchedOutline": {
              borderColor: "#60C2AA"
            }
          },
          "&.Mui-error": {
            "& .MuiOutlinedInput-notchedOutline": {
              borderColor: "#E93E3E !important"
            },
            ":hover": {
              "& .MuiOutlinedInput-notchedOutline": {
                borderColor: "#E93E3E !important"
              }
            }
          },
          "&.Mui-disabled": {
            "& .MuiOutlinedInput-notchedOutline": {
              borderColor: "#DCDCDF"
            },
            ":hover": {
              "& .MuiOutlinedInput-notchedOutline": {
                borderColor: "#DCDCDF"
              }
            }
          },
          "&.Mui-focused": {
            "& .MuiOutlinedInput-notchedOutline": {
              borderColor: "#3BA189"
            },
            ":hover": {
              "& .MuiOutlinedInput-notchedOutline": {
                borderColor: "#3BA189"
              }
            }
          }
        },
        focused: {
        },
        disabled: {
        },
        error: {
        },
        input: {
          padding: "12px 20px"
        },
        notchedOutline: {
          borderRadius: "12px",
          borderColor: "#DCDCDF"
        }
      }
    },
    MuiButton: {
      styleOverrides: {
        root: ({ownerState}) => {
          let width;

          if (ownerState.size === "medium") {
            width = "161px";
          }

          return ({
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            padding: "12px 20px",
            borderRadius: "12px",
            width,
            textTransform: "none",
            "&.Mui-disabled": {
              opacity: "0.3"
            }
          });
        }
      }
    },
    MuiCard: {
      styleOverrides: {
        root: {
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
          gap: "24px",
          padding: "32px",
          borderRadius: "40px",
          height: "fit-content",
          background: "#FFFFFF",
          boxShadow: "0 4px 20px rgba(44, 57, 121, 0.09)"
        }
      }
    },
    MuiCardHeader: {
      styleOverrides: {
        root: {
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          width: "100%",
          padding: "0"
        },
        title: {
          fontSize: 24,
          fontWeight: "700",
          lineHeight: "36px"
        }
      }
    },
    MuiCardContent: {
      styleOverrides: {
        root: {
          ":last-child": {
            padding: "0"
          }
        }
      }
    },
    MuiIcon: {
      styleOverrides: {
        root: {
          width: "max-content",
          height: "max-content"
        }
      }
    }
  }
});
