import React from "react";
import ReactDOM from "react-dom/client";
import {CssBaseline, ThemeProvider} from "@mui/material";
import {createBrowserHistory} from "history";

import "./core/api";

import {RouterStore, RouterStoreContext} from "./stores/router";
import {UserStore, UserStoreContext} from "./stores/user";

import App from "./app";
import {theme} from "./theme";


ReactDOM
  .createRoot(document.getElementById("root"))
  .render(
    <React.StrictMode>
      <ThemeProvider theme={theme}>
        <CssBaseline/>
        <RouterStoreContext.Provider value={new RouterStore(createBrowserHistory())}>
          <UserStoreContext.Provider value={new UserStore()}>
            <App/>
          </UserStoreContext.Provider>
        </RouterStoreContext.Provider>
      </ThemeProvider>
    </React.StrictMode>
  );
