import * as React from "react";
import {Navigate} from 'react-router';
import {UserStoreContext} from '../stores/user';

const withAuth = (Component) => (props) => {
  const userStore = React.useContext(UserStoreContext);

  return userStore.user ?
    <Component {...props} /> :
    <Navigate to={"/login"} replace={true} />;
};

export default withAuth;
