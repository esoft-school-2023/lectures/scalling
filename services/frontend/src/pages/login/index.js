import * as React from "react";
import {Box} from "@mui/material";
import {Navigate} from "react-router";
import {observer} from "mobx-react-lite";
import {UserStoreContext} from "../../stores/user";
import {LoginStore, LoginStoreContext} from "./store";
import LoginView from "./view";

const Login = () => {
  const userStore = React.useContext(UserStoreContext);
  const loginStore = React.useMemo(() => new LoginStore(userStore), [userStore]);

  if (userStore.user) {
    return <Navigate to={"/"} replace={true} />;
  }

  return (
    <LoginStoreContext.Provider value={loginStore}>
      <Box sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#F6F6F6",
        minHeight: "100vh"
      }}>
        <LoginView />
      </Box>
    </LoginStoreContext.Provider>
  );
};

export default observer(Login);
