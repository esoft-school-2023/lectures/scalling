import * as React from "react";
import * as mobx from "mobx";

export class LoginStore {
  userStore;

  username = "";
  password = "";
  errors = {
    username: "",
    password: "",
    global: ""
  };

  constructor(userStore) {
    mobx.makeObservable(this, {
      username: mobx.observable,
      password: mobx.observable,
      errors: mobx.observable,
      hasError: mobx.computed,
      onChangeUsername: mobx.action,
      onChangePassword: mobx.action,
      onSubmit: mobx.action
    });

    this.userStore = userStore;
  }

  get hasError() {
    return !!this.errors.username || !!this.errors.password;
  }

  onChangeUsername = (value) => {
    this.errors.username = "";
    this.errors.global = "";
    this.username = value;
  };

  onChangePassword = (value) => {
    this.errors.password = "";
    this.errors.global = "";
    this.password = value;
  };

  onSubmit = async() => {
    this.errors.username = !this.username.trim() ? "Введите логин" : "";
    this.errors.password = !this.password.trim() ? "Введите пароль" : "";

    if (this.hasError) {
      return;
    }

    try {
      await this.userStore.login(this.username, this.password);
    } catch(err) {
      mobx.runInAction(() => {
        if (err.message === "unauthorized") {
          this.errors.username = "Неверный логин или пароль";
          this.errors.password = "Неверный логин или пароль";
        } else {
          this.errors.global = "Произошла неизвестная ошибка";
        }
      });
    }
  };
}

export const LoginStoreContext = React.createContext(null);
