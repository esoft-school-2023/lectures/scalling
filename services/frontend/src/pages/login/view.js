import * as React from "react";
import {observer} from "mobx-react-lite";
import {Box, Typography} from "@mui/material";
import TextField from "../../components/text-field";
import Button from "../../components/button";
import Card from "../../components/card";
import {LoginStoreContext} from "./store";
import dogeImg from "./doge.png";

const LoginView = () => {
  const loginStore = React.useContext(LoginStoreContext);

  return (
    <Card
      sx={{
        padding: "40px 24px",
        borderRadius: "16px"
      }}
      contentSx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "center",
        gap: "20px",
        minWidth: "352px"
      }}
    >
      <Box sx={{marginLeft: "auto", marginRight: "auto"}}>
        <img src={dogeImg} alt={"Дог"} width={132} height={132}/>
      </Box>
      <Typography variant={"h1"} align={"center"}>
        {"Войдите в игру"}
      </Typography>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "stretch",
          justifyContent: "center",
          gap: "12px"
        }}
      >
        <TextField
          placeholder={"Логин"}
          defaultValue={loginStore.username}
          onChange={loginStore.onChangeUsername}
          error={!!loginStore.errors.username}
          helperText={loginStore.errors.username}
        />
        <TextField
          placeholder={"Пароль"}
          type={"password"}
          defaultValue={loginStore.password}
          onChange={loginStore.onChangePassword}
          error={!!loginStore.errors.password}
          helperText={loginStore.errors.password}
        />
      </Box>
      <Button fill={true} onClick={loginStore.onSubmit}>
        {"Войти"}
      </Button>
      {
        !!loginStore.errors.global && (
          <Typography variant={"small"} color={"error"}>
            {loginStore.errors.global}
          </Typography>
        )
      }
    </Card>
  );
};

export default observer(LoginView);
