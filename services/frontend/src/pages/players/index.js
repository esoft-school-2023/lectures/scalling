import * as React from "react";
import {observer} from 'mobx-react-lite';
import withAuth from "../../hoc/with-auth";
import {PlayersStore, PlayersStoreContext} from "./store";
import PlayersView from "./view";

const Players = () => {
  const playersStore = React.useMemo(() => new PlayersStore(), []);

  React.useEffect(() => {
    playersStore.init();

    return () => playersStore.dispose();
  }, [playersStore]);

  return (
    <PlayersStoreContext.Provider value={playersStore}>
      <PlayersView/>
    </PlayersStoreContext.Provider>
  );
};

export default observer(withAuth(Players));
