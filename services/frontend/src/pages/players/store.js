import * as React from "react";
import * as mobx from "mobx";
import playersApi from "../../api/players";

export class PlayersStore {
  players = [];
  interval = null;
  onlyFree = false;

  constructor() {
    mobx.makeObservable(this, {
      players: mobx.observable,
      onlyFree: mobx.observable,
      toggleOnlyFree: mobx.action
    });
  }

  toggleOnlyFree = () => {
    this.onlyFree = !this.onlyFree;
  };

  init = () => {
    this.interval = setInterval(() => this.loadPlayers(), 1000);
  }

  dispose = () =>  {
    clearInterval(this.interval);
  };

  loadPlayers = async() => {
    try {
      const players = await playersApi.getPlayers();

      mobx.runInAction(() => {
        this.players = players;
      });
    } catch(_) {
      this.players = [];
    }
  };
}

export const PlayersStoreContext = React.createContext(null);
