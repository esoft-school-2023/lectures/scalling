import * as React from "react";
import {Box, Typography, Switch} from "@mui/material";
import {observer} from "mobx-react-lite";
import Button from "../../components/button";
import Card from "../../components/card";
import Label from "../../components/label";
import Table from "../../components/table";
import {PlayersStoreContext} from "./store";

const columns = [
  {
    name: "fullName",
    valueSx: {
      width: "360px",
      padding: "0"
    },
    format: ({fullName}) => fullName
  },
  {
    name: "status",
    valueSx: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "flex-end",
      alignItems: "center",
      gap: "80px",
      padding: "0",
      marginLeft: "16px"
    },
    format: ({isBusy, invite}) => (
      <React.Fragment>
        <Label
          sx={{width: "100px"}}
          color={isBusy ? "warning" : "success"}
        >
          {isBusy ? "В игре" : "Свободен"}
        </Label>
        <Button
          color={"primary"}
          disabled={!!isBusy}
          onClick={invite}
        >
          {"Позвать играть"}
        </Button>
      </React.Fragment>
    )
  }
];

const PlayersView = () => {
  const playersStore = React.useContext(PlayersStoreContext);

  return (
    <Card
      header={
        <Box sx={{
          display: "flex",
          justifyContent: "space-between",
          width: "100%"
        }}>
          <Typography variant={"h1"}>{"Список игроков"}</Typography>
          <Box sx={{
            display: "flex",
            alignItems: "center"
          }}>
            <Typography>{"Только активные"}</Typography>
            <Switch
              checked={playersStore.onlyFree}
              onChange={playersStore.toggleOnlyFree}
            />
          </Box>
        </Box>
      }
    >
      <Table
        sx={{width: "781px"}}
        columns={columns}
        withHead={false}
        rowKey={"id"}
        rows={playersStore.players}
      />
    </Card>
  );
};

export default observer(PlayersView);
