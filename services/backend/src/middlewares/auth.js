const jwt = require("jsonwebtoken");

exports.express = (secret) => (req, res, next) => {
  const [schema = "", token] = req.headers.authorization?.split(" ") ?? [];

  if (schema.toLowerCase() !== "bearer") {
    return res
      .status(401)
      .json({message: "unauthorized"});
  }

  try {
    const {sub: id, name: fullName} = jwt.verify(token, secret);

    req.user = {id, fullName};

    return next();
  } catch (_) {
    return res
      .status(401)
      .json({message: "unauthorized"});
  }
};

exports.socketIo = (secret, socket) => {
  const requestAuthorize = (error) => {
    socket.timeout(1000).emit("unauthorized", error, (err, token) => {
      if (err) {
        return socket.disconnect();
      }

      socket.data.token = token;

      try {
        const {sub: id, name: fullName} = jwt.verify(token, secret);

        if (!socket.data.user) {
          socket.data.user = {id, fullName};
          socket.data.isBusy = false;
          socket.join(id);
        } else if (socket.data.user.id !== id) {
          // somebody tried hack us?
          socket.disconnect();
        }
      } catch(err) {
        socket.data.token = null;

        requestAuthorize("invalid_token");
      }
    });
  };

  socket.use(async(_, next) => {
    try {
      jwt.verify(socket.data.token, secret)

      next();
    } catch(err) {
      next(new Error("unauthorized"));
    }
  });

  socket.on("error", (err) => {
    if (err?.message === "unauthorized") {
      requestAuthorize(null);
    }
  });

  requestAuthorize(null);
};
