const authHandler = require("../handlers/auth");

exports.login = (ext) => async(req, res) => {
  try {
    const {type} = req.body;
    let authPromise;

    if (type === "credentials") {
      const {username, password} = req.body;

      authPromise = authHandler.loginByCredentials(username, password, ext);
    } else if (type === "refreshToken") {
      const {refreshToken: oldRefreshToken} = req.signedCookies;

      authPromise = authHandler.loginByRefresh(oldRefreshToken, ext);
    } else {
      return res
        .status(401)
        .json({message: "unauthorized"});
    }

    const {accessToken, refreshToken} = await authPromise;

    return res
      .status(201)
      .cookie("refreshToken", refreshToken, ext.config.cookie.params)
      .json({accessToken});
  } catch (_) {
    return res
      .status(401)
      .json({message: "unauthorized"});
  }
};

exports.logout = (ext) => async(req, res) => {
  try {
    await authHandler.logoutUser(req.user.id, ext);

    return res
      .status(204)
      .clearCookie("refreshToken")
      .json();
  } catch (_) {
    return res
      .status(500)
      .json({message: "unknown_error"});
  }
};

exports.resource = (ext) => async(req, res) => {
  try {
    const user = await authHandler.getUser(req.user, ext);

    return res
      .status(200)
      .json(user);
  } catch(_) {
    return res
      .status(500)
      .json({message: "unknown_error"});
  }
};
