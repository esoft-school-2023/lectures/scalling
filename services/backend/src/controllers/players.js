const playersHandler = require("../handlers/players");

exports.getPlayers = (socket, ext) => async(onlyFree, cb) => {
  try {
    const players = await playersHandler.getPlayers(socket.data.user.id, onlyFree, ext);

    cb(players);
  } catch (_) {
    cb({message: "unknown_error"});
  }
};
