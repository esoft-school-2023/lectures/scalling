exports.getPlayers = async(currentUserId, onlyFree, {io}) => {
  const sockets = await io.fetchSockets();
  const playersById = sockets.reduce((result, socket) => {
    const {user} = socket.data;

    if (currentUserId === user.id) {
      return result;
    }

    if (!result.has(user.id)) {
      result.set(user.id, {
        ...user,
        isBusy: socket.data.isBusy
      });
    } else {
      const {isBusy} = result.get(user.id);

      result.set(user.id, {
        ...user,
        isBusy
      });
    }

    return result;
  }, new Map());

  const players = [...playersById.values()];

  if (!onlyFree) {
    return players;
  }

  return players.filter((player) => !player.isBusy);
};
