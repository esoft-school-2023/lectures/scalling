const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

exports.loginByCredentials = async(username, password, {config, db}) => {
  const user = await db("users")
    .where("username", username)
    .select(["id", "fullName", "password"])
    .first();
  const isPasswordMatches = await bcrypt.compare(password, user.password);

  if (!isPasswordMatches) {
    throw new Error();
  }

  return {
    accessToken: jwt.sign(
      {sub: user.id, name: user.fullName},
      config.jwt.secret,
      config.jwt.accessToken
    ),
    refreshToken: jwt.sign(
      {sub: user.id},
      config.jwt.secret,
      config.jwt.refreshToken
    )
  };
};

exports.loginByRefresh = async(refreshToken, {config, db}) => {
  const {sub: userId, iat: issuedAt} = jwt.verify(refreshToken, config.jwt.secret);
  const user = await db("users")
    .where("id", userId)
    .select(["id", "fullName", "lastLogoutAt"])
    .first();

  if (new Date(issuedAt * 1000) <= new Date(user.lastLogoutAt))  {
    throw new Error();
  }

  return {
    accessToken: jwt.sign(
      {sub: user.id, name: user.fullName},
      config.jwt.secret,
      config.jwt.accessToken
    ),
    refreshToken
  };
};

exports.logoutUser = async(userId, {db, io}) => {
  await db("users")
    .where("id", userId)
    .update({lastLogoutAt: new Date().toISOString()});
  await io.in(userId).disconnectSockets();
};

exports.getUser = (user) => user;
