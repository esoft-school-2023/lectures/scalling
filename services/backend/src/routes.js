const express = require("express");

const authController = require("./controllers/auth");

exports.auth = (protectedRoute, ext) => {
  const router = express.Router();

  router.post("/", authController.login(ext));
  router.delete("/", protectedRoute, authController.logout(ext));
  router.get("/", protectedRoute, authController.resource(ext));

  return router;
};
