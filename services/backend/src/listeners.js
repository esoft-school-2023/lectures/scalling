const playersController = require("./controllers/players");

exports.players = (socket, ext) => {
  socket.on("players:get-players", playersController.getPlayers(socket, ext));
};
