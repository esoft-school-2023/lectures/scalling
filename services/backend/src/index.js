const http = require("http");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const express = require("express");
const knex = require("knex");
const socketIo = require("socket.io");
const _ = require("lodash");

const routes = require("./routes");
const listeners = require("./listeners");
const authMiddleware = require("./middlewares/auth");

exports.run = (config) => {
  const db = knex({
    ...config.db,
    wrapIdentifier: (value, origImpl) => origImpl(_.snakeCase(value)),
    postProcessResponse: (result) => {
      if (!result) {
        return result;
      }
      if (Array.isArray(result)) {
        return result.map((row) => _.mapKeys(row, (_value, key) => _.camelCase(key)));
      }

      return _.mapKeys(result, (_value, key) => _.camelCase(key));
    }
  });
  const app = express();
  const server = http.createServer(app);
  const io = new socketIo.Server(server, {cors: config.cors});
  const protectedRoute = authMiddleware.express(config.jwt.secret);
  const ext = {config, db, io};

  app.use(express.json());
  app.use(cors(config.cors));
  app.use(cookieParser(config.cookie.secret));
  app.use("/auth", routes.auth(protectedRoute, ext));

  io.on("connection", (socket) => {
    authMiddleware.socketIo(config.jwt.secret, socket);
    listeners.players(socket, ext);
  });

  server.listen(config.port);
};
