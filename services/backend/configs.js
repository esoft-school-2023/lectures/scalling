const tryParse = (value) => {
  try {
    return JSON.parse(value);
  } catch(err) {
    return value;
  }
};

exports.development = {
  port: 5000,
  jwt: {
    secret: "super-secret-key",
    accessToken: {
      expiresIn: "1h"
    },
    refreshToken: {
      expiresIn: "24h"
    }
  },
  cors: {
    origin: ["http://localhost:3000", "http://frontend:3000", "http://localhost:5000", "http://backend:5000"],
    credentials: true
  },
  cookie: {
    secret: "super-secret-key",
    params: {
      sameSite: false,
      secure: false,
      signed: true,
      httpOnly: true,
      maxAge: 24 * 60 * 60 * 1000
    }
  },
  db: {
    client: "pg",
    connection: "postgresql://tic_tac_toe:Passw0rd@pgsql:5432/tic_tac_toe?application_name=tic-tac-toe",
    migrations: {
      tableName: "migrations",
      directory: "./db/migrations"
    },
    seeds: {
      directory: "./db/seeds"
    }
  }
};

exports.production = {
  port: process.env.PORT,
  jwt: {
    secret: process.env.JWT_SECRET,
    accessToken: {
      expiresIn: process.env.ACCESS_TOKEN_EXPIRATION_TIME
    },
    refreshToken: {
      expiresIn: process.env.REFRESH_TOKEN_EXPIRATION_TIME
    }
  },
  cookie: {
    secret: process.env.COOKIE_SECRET,
    params: {
      sameSite: true,
      secure: true,
      signed: true,
      httpOnly: true,
      maxAge: process.env.COOKIE_MAX_AGE
    }
  },
  cors: {
    origin: tryParse(process.env.CORS_ORIGIN),
    credentials: true
  },
  db: {
    client: "pg",
    connection: process.env.DB_URL,
    migrations: {
      tableName: "migrations",
      directory: "./db/migrations"
    },
    seeds: {
      directory: "./db/seeds"
    }
  }
};
