const configs = require("./configs");
const {run} = require("./src");

run(configs[process.env.NODE_ENV || "development"]);
