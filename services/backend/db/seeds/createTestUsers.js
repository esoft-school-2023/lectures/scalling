const bcrypt = require("bcrypt");

const users = [
  {
    username: "test1",
    password: "Passw0rd",
    full_name: "Тест Первый",
    created_at: new Date().toISOString(),
    last_logout_at: new Date().toISOString()
  },
  {
    username: "test2",
    password: "Passw0rd",
    full_name: "Тест Второй",
    created_at: new Date().toISOString(),
    last_logout_at: new Date().toISOString()
  },
  {
    username: "test3",
    password: "Passw0rd",
    full_name: "Тест Третий",
    created_at: new Date().toISOString(),
    last_logout_at: new Date().toISOString()
  },
  {
    username: "test4",
    password: "Passw0rd",
    full_name: "Тест Четвертый",
    created_at: new Date().toISOString(),
    last_logout_at: new Date().toISOString()
  }
];

exports.seed = async(knex) => {
  await knex('users').del();
  await Promise.all(users.map(async(user) => {
    user.password = await bcrypt.hash(user.password, 6);
  }));
  await knex('users').insert(users);
};
