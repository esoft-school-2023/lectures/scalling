exports.up = (knex) => knex.schema.createTable("users", (table) => {
  table
    .increments("id")
    .primary();
  table
    .string("username")
    .notNullable()
    .unique();
  table
    .string("password")
    .notNullable();
  table
    .string("full_name")
    .notNullable();
  table
    .timestamp("created_at", {useTz: false})
    .notNullable();
  table
    .timestamp("last_logout_at", {useTz: false})
    .notNullable();
});

exports.down = (knex) => knex.schema.dropTable("users");
