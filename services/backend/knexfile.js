const configs = require("./configs");

module.exports = {
  development: configs.development.db,
  production: configs.production.db
};
